from .consolidate_facts import get_consolidate_facts

__all__ = ['get_consolidate_facts']
