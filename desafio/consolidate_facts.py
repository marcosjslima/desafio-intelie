"""Implements a function that consolidate facts accordingly to a schema
   and an active flag."""


def cardinality(attribute, schema):
    """
    Returns the cardinality of an attribute accordingly with the
    specified schema
    :param attribute: the attribute to get it's cardinality
    :param schema: the schema where this attribute must be present with
                   the cardinality
    :return: the attribute's cardinality
    """
    return list(
        filter(lambda x: x[0] == attribute and x[1] == 'cardinality',
               schema))[0][2]


def remove_value(consolidated, key, value):
    """
    Remove a value from the consolidated dict. If the list of values become
        empty, remove the entire key from the dict.
    :param consolidated: The consolidated dict being constructed
    :param key: The tuple entity-attribute to remove a value from
    :param value: The value to be removed
    :return: None
    """
    if value in consolidated[key]['values']:
        consolidated[key]['values'].remove(value)
        if not consolidated[key]['values']:
            del consolidated[key]


def update_consolidated_item(consolidated, key, value, remove):
    """
    Update the consolidated dict, appending or assingning the value to
        the key on the dict, accordingly with the cardinality of the attribute.
    :param consolidated: The consolidated dict being constructed.
    :param key: A tuple containing an entity and an attribute
    :param value: The value to be appended or assigned to the key.
    :param remove: A flag indicating to remove this value fom the dict.
    :return: None
    """
    if remove:
        remove_value(consolidated, key, value)
    else:
        if consolidated[key]['cardinality'] == 'one':
            consolidated[key]['values'] = [value]
        else:
            consolidated[key]['values'].append(value)


def add_consolidated_item(consolidated, key, value, schema):
    """
    Add the first appearance of some key on the consolidated dict.
    :param consolidated: The consolidated dict being constructed
    :param key: A tuple containing an entity and an attribute
    :param value: The value of this attribute to this entity
    :param schema: The schema containing all attribute's cardinality
    :return: None
    """
    attr = key[1]
    consolidated.update({key: {'values': [value],
                               'cardinality': cardinality(attr, schema)}})


def consolidate_facts(facts, schema):
    """
    Construct a dict where the key is a tuple of the entity and attribute, and
        the value is a dict with a list of values and the attribute
        cardinality. If the cardinality is 'many', this list will be appended
        with many values of this entity's attribute. If the cardinality is
        'one', the list will be always assigned the last value ot this entity's
        attribute.
    :param facts: All facts with active flag on (True)
    :param schema: The schema containing all attributes cardinalities.
    :return: The consolidated dict to be open to the final list.
    """
    consolidated = {}
    for i, x in enumerate(facts):
        key = (x[0], x[1],)
        if key in consolidated:
            update_consolidated_item(consolidated, key, x[2], not x[3])
        elif x[3]:
            add_consolidated_item(consolidated, key, x[2], schema)

    return consolidated


def open_consolidated_dict(consolidated):
    """
    Open the consolidated dict to the final list of facts, consolidated.
    :param consolidated: The consolidated dict constructed.
    :return: The final list of consolidated facts
    """
    consolidated_list = []

    for it in list(consolidated.items()):
        for valor in it[1]['values']:
            consolidated_list.append((it[0][0], it[0][1], valor, True))

    return consolidated_list


def get_consolidate_facts(facts, schema):
    """
    Consolidate facts accordingly with schema cardinality and active flag
    :param facts: list of tuples containing:
            [entity, attribute, value, active_flag]
    :param schema: list of tuples containing
            [attribute, 'cardinality', 'one'/'many']
    :return: consolidated facts
    """

    consolidated_dict = consolidate_facts(facts, schema)

    consolidated_list = open_consolidated_dict(consolidated_dict)

    return consolidated_list
