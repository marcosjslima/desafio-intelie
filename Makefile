.PHONY: tests clean install help

help:
	@echo "make tests: make tests with coverage report, cyclomatic complexity analisys and pep8"
	@echo "make clean: remove pytest, python caches and coverage files"
	@echo "make install: install requirements_dev.txt packages."

tests:
	@python -m pytest -vv --cov=. --cov=desafio --cov=tests --cov-report html --cov-report term-missing:skip-covered
	@flake8 desafio/ --max-complexity=5
	@flake8 tests --ignore=S101,S311

install:
	@pip install -r requirements_dev.txt

clean: clean-pyc clean-test

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test:
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr .pytest_cache
