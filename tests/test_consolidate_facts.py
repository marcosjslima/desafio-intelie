from desafio import get_consolidate_facts


def test_consolidate_facts(facts, schema):
    expected = [('gabriel', 'endereço', 'av rio branco, 109', True),
                ('joão', 'endereço', 'rua bob, 88', True),
                ('joão', 'telefone', '91234-5555', True),
                ('gabriel', 'telefone', '98888-1111', True),
                ('gabriel', 'telefone', '56789-1010', True)]

    consolidated = get_consolidate_facts(facts, schema)

    assert consolidated == expected


def test_consolidate_facts_remove_key(facts_causing_delete_key, schema):
    expected = [('gabriel', 'endereço', 'av rio branco, 109', True),
                ('joão', 'endereço', 'rua bob, 88', True),
                ('joão', 'telefone', '91234-5555', True)]

    consolidated = get_consolidate_facts(facts_causing_delete_key, schema)

    assert consolidated == expected
