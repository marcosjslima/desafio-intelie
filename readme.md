# Desafio Intellie
## by Marcos Lima

#Instruções:
1. Crie um virtualenv com python3

    ```bash
    mkvirtualenv desafio -p python3
    ```
    
2. Instale as dependências (pode usar make)

    ```bash
    make install
    ``` 
    
    ou
    
    ```bash
    pip install -r requirements_dev.txt
    ```

3. Execute os testes.

    ```bash
    make tests
    ```
